#!/bin/bash

#npm run build
cd build
tar -cvf build.tar .
scp build.tar ryan@music.home:/home/ryan/audio-docker/server
rm build.tar
