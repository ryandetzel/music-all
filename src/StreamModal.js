import React, { Component } from 'react';
import { connect } from 'react-redux'

import Stream from './Stream'
import Playlist from './Playlist'
import { types } from './appRedux'
import './StreamModal.css';

class StreamModal extends Component {
  onHideStreamModal = () => this.props.onHideStreamModal()

  
  availableStreams = () => {
    const { streams } = this.props
    const { client } = this.props.streamModal
    const clientStream = client.stream

    const aStreams = streams.filter(function(stream){
      return (stream.playback === "playing" && (!clientStream || stream.id !== clientStream.id))
    })
    return aStreams
  }

  render(){
    const {playlists, streamModal} = this.props
    const client = streamModal.client

    if(!streamModal.visible) {
      return null;
    }

    const streams = this.availableStreams()

    return (
    <div className="streamModal" onClick={this.onHideStreamModal}>
      {/* <button className="cancel" onClick={this.onHideStreamModal}>Close</button> */}

      <h3>Join Stream</h3>
        { streams && streams.map(function(stream, index){
            return <div className="stream" key={index}>
              <Stream clientId={client.id} {...stream} />
            </div>
        }, this)}

      <h3>New Stream</h3>
        {playlists && playlists.map(function(playlist, index){
          return <div className="playlist" key={index}>
              <Playlist clientId={client.id} {...playlist} />
            </div>
        })}

      
    </div>)
  }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = dispatch => {
  return {
    onHideStreamModal: () => dispatch({
      type: types.HIDE_STREAM_MODAL
    })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StreamModal)
