import React, { Component } from 'react';
import { connect } from 'react-redux'

import { controlActions, controllerAction} from './actionHelper'
import { types } from './appRedux'

import './Controller.css';

class Controller extends Component {
  onPause = () => this.props.onAction(this.props.id, controlActions.pause);
  onPlay = () => this.props.onAction(this.props.id, controlActions.play);
  onPreviousTrack = () => this.props.onAction(this.props.id, controlActions.previous);
  onNextTrack = () => this.props.onAction(this.props.id, controlActions.next);
  onVolumeDown = () => this.props.onAction(this.props.id, controlActions.volume_down, this.props.volume)
  onVolumeUp = () => this.props.onAction(this.props.id, controlActions.volume_up, this.props.volume)
  onUnMute = () => this.props.onAction(this.props.id, controlActions.unmute)
  onMute = () => this.props.onAction(this.props.id, controlActions.mute)
  onShowStreamModal = () => this.props.onShowStreamModal(this.props)
  onTurnOff = () => this.props.onAction(this.props.id, controlActions.off)

  render() {
    const client = this.props
    const stream = client.stream

    const isPlaying = stream && stream.playback === "playing"
    const isMuted = client.muted
    
    const buttonsDisabled = stream && stream.uri && !stream.uri.startsWith("spotify")

    const Background = stream && client.connected ? stream.image : ""

    return (
      <div className="controller">        
        <div>
          <div className="top" style={{backgroundImage: "url(" + Background + ")"}} onClick={this.onShowStreamModal}>
            <div className="trackInfo">
              <h1>{client.name}</h1>
              {client.connected ? (
                stream ? (
                <div>
                  <h3>{stream && stream.name}</h3>
                  <h4>{stream && stream.artist}</h4>
                </div>
                 ) : (<em>No Stream Playing</em>)
              ) : (
                <em>Offline</em>
              )}
            </div>
          </div>   

         <div className="controls">
            <button className="previousButton" disabled={buttonsDisabled} onClick={this.onPreviousTrack}></button>

            {isPlaying ? (
              <button  className="pauseButton" onClick={this.onPause}></button>
            ):(
              <button  className="playButton" onClick={this.onPlay}></button>
            )}

            <button className="nextButton" disabled={buttonsDisabled} onClick={this.onNextTrack}></button>
         </div>

         <div>
            <button className="volumeDownButton" onClick={this.onVolumeDown}></button>

            {isMuted ? (
              <button className="muteButton muted" onClick={this.onUnMute}></button>
            ) : (
              <button className="muteButton" onClick={this.onMute}></button>
            )}

            <button className="volumeUpButton" onClick={this.onVolumeUp}></button>
         </div>
         </div>
          <div>
                   <button className="offButton" onClick={this.onTurnOff} disabled></button>
                   <div className="volume">{client.volume}</div>
          </div>
       </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAction: (clientId, action, value="") => dispatch(
      controllerAction(clientId, action, value)
    ),
    onShowStreamModal: (client) => dispatch({
      type: types.SHOW_STREAM_MODAL,
      client: client
    })
  }
}

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(Controller)
