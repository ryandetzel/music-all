// The types of actions that you can dispatch to modify the state of the store
export const types = {
  UPDATE_STATE: "UPDATE_STATE",
  UPDATE_MQTT_CLIENT: "UPDATE_MQTT_CLIENT",
  SHOW_STREAM_MODAL: "SHOW_STREAM_MODAL",
  HIDE_STREAM_MODAL: "HIDE_STREAM_MODAL"
};

//const jsonStr = '{"streams": {"stream3": {"name": "Greatest Love Story", "id": "stream3", "playback": "stopped", "timestamp": 1512508244, "images": ["https://i.scdn.co/image/3ef2b484579ebe5639e24ac2e0b19390314ce555"], "uri": "spotify:track:0kdiUZDcLIFw0Buo9r0Q4z", "artist": "LANCO"}, "stream2": {"name": "O Tannenbaum", "id": "stream2", "playback": "playing", "timestamp": 1512516762, "images": ["https://i.scdn.co/image/034874a48c34401f4ebc0ed9f5e01c88b2eb9ade"], "uri": "spotify:track:0bqtMZ5WgbKIl1rp9khuVz", "artist": "Nat King Cole"}, "stream4": {"name": "Shhhhh. Calming sounds to help wash away the worries of the day", "id": "stream4", "playback": "stopped", "timestamp": 1512508244, "images": ["http://cdn-profiles.tunein.com/s249937/images/logoq.png"], "uri": "tunein:station:s249937", "artist": "Relax and Unwind"}, "stream1": {"name": "Wish I Was Sober", "id": "stream1", "playback": "playing", "timestamp": 1512516824, "images": ["https://i.scdn.co/image/7ae081c3f2d14ba8b8345b067e9ce4175ccf4a44"], "uri": "spotify:track:09YA1Ma4NhmKCu9VHdQMcU", "artist": "Rhys Lewis"}, "stream5": {"name": "Your Lifestyle, Your Music", "id": "stream5", "playback": "stopped", "timestamp": 1512508244, "images": ["http://cdn-radiotime-logos.tunein.com/s113658q.png"], "uri": "tunein:station:s113658", "artist": "181.FM Beatles"}}, "playlists": [], "clients": [{"stream": {"name": "O Tannenbaum", "id": "stream2", "playback": "playing", "timestamp": 1512516762, "images": ["https://i.scdn.co/image/034874a48c34401f4ebc0ed9f5e01c88b2eb9ade"], "uri": "spotify:track:0bqtMZ5WgbKIl1rp9khuVz", "artist": "Nat King Cole"}, "volume": 10, "sc_id": "c13065a5d915416ea753bf1ac16a85ae", "id": "piplayer1", "connected": true, "muted": false}, {"stream": {"name": "Wish I Was Sober", "id": "stream1", "playback": "playing", "timestamp": 1512516824, "images": ["https://i.scdn.co/image/7ae081c3f2d14ba8b8345b067e9ce4175ccf4a44"], "uri": "spotify:track:09YA1Ma4NhmKCu9VHdQMcU", "artist": "Rhys Lewis"}, "volume": 20, "sc_id": "29938206-6147-5EEE-AE8A-9F6E0B1186DE", "id": "ghost", "connected": true, "muted": false}, {"stream": {"name": "O Tannenbaum", "id": "stream2", "playback": "paused", "timestamp": 1512516762, "images": ["https://i.scdn.co/image/034874a48c34401f4ebc0ed9f5e01c88b2eb9ade"], "uri": "spotify:track:0bqtMZ5WgbKIl1rp9khuVz", "artist": "Nat King Cole"}, "volume": 10, "sc_id": "8c5604e85c924195aef34a3e4079998d", "id": "pi", "connected": true, "muted": false}]}';
//const jsonObj = JSON.parse(jsonStr)

const initialState =  {
  playlists: [],
  streamModal: {
    visible: false
  }
}

export const reducer = (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case types.UPDATE_MQTT_CLIENT: {
      return {
        ...state,
        mqttClient: action.mqttClient
      }
    }
    case types.UPDATE_STATE: {
      return {
        ...state,
        clients: action.state.clients,
        streams: action.state.streams,
        playlists: action.state.playlists
      }
    }
    case types.SHOW_STREAM_MODAL: {
      return {
        ...state,
        streamModal: {
          visible: true,
          client: action.client
        }
      }
    }
    case types.HIDE_STREAM_MODAL: {
      return {
        ...state,
        streamModal: {
          visible: false
        }
      }
    }
    default: return state
  }
};
